package in.teze.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.SeekBar;

/**功能：
 * TzSeekBar
 * @author   by fooyou 2014年7月29日   上午9:40:45
 */
public class TzSeekBar extends SeekBar {

	
	private boolean isEnableSeek=false;
	
	public TzSeekBar(Context context) {
		super(context);
	}

	public TzSeekBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public TzSeekBar(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (isEnableSeek) {
			return super.onTouchEvent(event);
		}else {
			return true;
		}
	}

	public boolean isEnableSeek() {
		return isEnableSeek;
	}

	public void setEnableSeek(boolean isEnableSeek) {
		this.isEnableSeek = isEnableSeek;
	}
	
	
	
}
