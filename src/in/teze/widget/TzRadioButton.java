package in.teze.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.CompoundButton;
import android.widget.RadioButton;

public class TzRadioButton extends CompoundButton{

	public TzRadioButton(Context context) {
		super(context);
	}

	public TzRadioButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public TzRadioButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void toggle() {
		super.toggle();//hack it can toggle whether if it already toggled
	}
	

    @Override
    public void onInitializeAccessibilityEvent(AccessibilityEvent event) {
        super.onInitializeAccessibilityEvent(event);
        event.setClassName(RadioButton.class.getName());
    }

    @Override
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo info) {
        super.onInitializeAccessibilityNodeInfo(info);
        info.setClassName(RadioButton.class.getName());
    }
}
