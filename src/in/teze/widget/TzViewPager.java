package in.teze.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class TzViewPager extends ViewPager{

	private boolean isEanbleDrag=false;
	
	public TzViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public TzViewPager(Context context) {
		super(context);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent arg0) {
		if (isEanbleDrag) {
			return super.onTouchEvent(arg0);
		}else {
			return true;
		}
	}

	public boolean isEanbleDrag() {
		return isEanbleDrag;
	}

	public void setEanbleDrag(boolean isEanbleDrag) {
		this.isEanbleDrag = isEanbleDrag;
	}
	
	

	
}
